/**
 * @author Shawn
 */
public interface ChatServer {
    void send(String name, String text);
    void receive(String text);
    void register();
    void unregister();
}
