import communication.MessageManager;
import communication.MethodCallMessage;
import communication.NetworkAddress;

/**
 * @author Shawn
 */
public class ChatServerStub implements ChatServer {
    private final NetworkAddress textSenderAddress;
    private final MessageManager messageManager;
    private final NetworkAddress skeletonAddress;
    private TextReceiver receiver;

    public ChatServerStub(NetworkAddress textSenderAddress, NetworkAddress skeletonAddress) {
        this.textSenderAddress = textSenderAddress;
        this.messageManager = new MessageManager();
        this.skeletonAddress = skeletonAddress;
    }

    public void setReceiver(TextReceiver receiver) {
        this.receiver = receiver;
    }

    /**
     * Waits for a reply and checks if it contains no return-value.
     */
    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            System.out.println("Checking empty reply");
            MethodCallMessage reply = messageManager.wReceive();
            System.out.println(reply);
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }


    @Override
    public void send(String name, String text) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "send");
        message.setParameter("text",text);
        message.setParameter("name",name);
        messageManager.send(message, textSenderAddress);
        //checkEmptyReply();
    }

    @Override
    public void receive(String text) {

    }

    @Override
    public void register() {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "register");
        message.setParameter("skeleton", skeletonAddress.toString());
        messageManager.send(message, textSenderAddress);
    }

    @Override
    public void unregister() {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(), "unregister");
        message.setParameter("skeleton", skeletonAddress.toString());
        messageManager.send(message, textSenderAddress);
        System.out.println("Checking reply");
    }
}
