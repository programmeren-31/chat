package be.kdg;

public interface ChatClient {
    String getName();
    public void receive(String text);
}
