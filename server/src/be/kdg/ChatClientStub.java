package be.kdg;

import be.kdg.communication.MessageManager;
import be.kdg.communication.MethodCallMessage;
import be.kdg.communication.NetworkAddress;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * @author Shawn
 */
public class ChatClientStub implements ChatClient {
    private final NetworkAddress clientAddress;
    private final MessageManager messageManager;
    private static final String MESSAGE_FORMAT = "[%s] %s";
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");


    public ChatClientStub(NetworkAddress clientAddress) {
        this.clientAddress = clientAddress;
        this.messageManager = new MessageManager(0);
    }

    /**
     * Waits for a reply and checks if it contains no return-value.
     */
    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            value = reply.getParameter("result");
        }
    }



    @Override
    public String getName() {
        System.out.println("Getting name");
        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), "getname");
        messageManager.send(msg, clientAddress);
        MethodCallMessage reply = messageManager.wReceive();
        if (!"getname".equals(reply.getMethodName())) return "Unknown";
        System.out.println("Got name");
        return reply.getParameter("name");
    }

    @Override
    public void receive(String text) {
        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), "receive");
        msg.setParameter("text", String.format(MESSAGE_FORMAT, dtf.format(LocalDateTime.now()), text));
        messageManager.send(msg, clientAddress);
        checkEmptyReply();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatClientStub that = (ChatClientStub) o;
        return clientAddress.equals(that.clientAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientAddress);
    }
}
