package be.kdg;

import be.kdg.communication.NetworkAddress;

import java.util.*;

/**
 * @author Shawn
 */
public class ChatServerImpl implements ChatServer {
    private List<ChatClient> clients;

    public ChatServerImpl() {
        this.clients = new ArrayList<>();
    }

    @Override
    public void register(ChatClient client) {
        clients.add(client);
        send("server", client.getName() + " has entered the room");
    }

    @Override
    public void unregister(ChatClient client) {
        clients.remove(client);
        send("server", client.getName() + " has left the room");
    }

    @Override
    public void send(String name, String message) {
        for(ChatClient client : clients) {
            client.receive(name + ": " + message);
        }
    }
    /*List<ChatClient> clients = new LinkedList<>();

    @Override
    public void send(String text) {
        clients.forEach(c -> c.receive(text));
    }

    @Override
    public void register(String id) {
        System.out.println("Registring client with id =" + id );
        clients.add(new ChatServerStub(getAddress(id)));
    }

    @Override
    public void unregister(String id) {
        System.out.println("Unregistring client with ip = " + id + ", client size before deletion: " + clients.size());
        clients.remove(new ChatServerStub(getAddress(id)));
        System.out.println("Unregistring complete, client size: " + clients.size());
    }*/

    private NetworkAddress getAddress(String text) {
        String[] stringArr = text.split(":");
        return new NetworkAddress(stringArr[0], Integer.parseInt(stringArr[1]));
    }
}
